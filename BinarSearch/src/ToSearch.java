public class ToSearch {
    private int el;
    private int []arr2;

    public ToSearch(int []mas, int n){
        this.el = n;
        this.arr2 = mas;
    }

    public int bin(){
        int i = -1;

        int low = 0, high = arr2.length, mid;
        while (low < high) {
            mid = (low + high) / 2;
            if (el == this.arr2[mid]) {
                i = mid;
                break;
            } else {
                if (el < this.arr2[mid]) {
                    high = mid;
                } else {
                    low = mid + 1;
                }
            }
        }
        return i;
    }


}
