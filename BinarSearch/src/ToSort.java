public class ToSort {
    private int []arr;

    public ToSort (int []mas){
        this.arr = mas;
    }

    public int []bubble(){
        int []a = arr;
        for(int i=1; i<a.length; i++){
            for(int j = a.length-1; j>=i; j--){
                if (a[j - 1] > a[j]){
                    int temp = a[j];
                    a[j]=a[j-1];
                    a[j-1]=temp;
                }
            }
        }
        return a;
    }

}
